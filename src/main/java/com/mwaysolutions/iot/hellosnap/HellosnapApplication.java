package com.mwaysolutions.iot.hellosnap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellosnapApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellosnapApplication.class, args);
	}
}
